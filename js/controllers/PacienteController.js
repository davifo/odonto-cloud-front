angular.module('MetronicApp').controller('PacienteController',
    function ($rootScope, $scope, $http, $timeout, $modal, $log,PacienteService) {
    console.log('PacienteController');

    $scope.teste = PacienteService.getPaciente();
    console.log( $scope.teste );
    Date.prototype.ddmmyyyyhm = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = this.getDate().toString();
        var h = this.getHours(); // => 9
        var m = this.getMinutes(); // =>  30
        return (dd[1] ? dd : "0" + dd[0]) + (mm[1] ? mm : "0" + mm[0]) + +yyyy + " " + h + ":" + m; // padding
    };
    $scope.status = {};
    $scope.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
    };

    // set default layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.$on('$viewContentLoaded', function () {
        App.initAjax(); // initialize core components
    });

    $scope.novoProcedimento = {};
    $scope.novoProcedimento.data = new Date().ddmmyyyyhm();
    $scope.paciente = {};

    $scope.peopleAsync = [];


    $scope.counter = 0;
    $scope.someFunction = function (item, model) {
        $scope.counter++;
        $scope.eventResult = {
            item: item,
            model: model
        };
    };

    $scope.removed = function (item, model) {
        $scope.lastRemoved = {
            item: item,
            model: model
        };
    };

    $scope.person = {};
    $scope.faces = [{
        name: 'O',
        desc: 'Oclusal'
    }, {
        name: 'V',
        desc: 'Vestibular'
    }, {
        name: 'L',
        desc: 'Lingual'
    }, {
        name: 'M',
        desc: 'Mesial'
    }, {
        name: 'D',
        desc: 'Distal'
    }];

    $scope.dentes = [{
        name: '33',
        desc: ''
    }, {
        name: '55',
        desc: ''
    }, {
        name: '15',
        desc: ''
    }, {
        name: '13',
        desc: ''
    }, {
        name: '23',
        desc: ''
    }];

    $scope.filtrosProcedimentos = [{
        name: 'Todos',
        desc: ''
    }, {
        name: 'Pendentes',
        desc: ''
    }, {
        name: 'Concluídos',
        desc: ''
    }, {
        name: 'Dente 12',
        desc: ''
    }, {
        name: 'Dente 23',
        desc: ''
    }];

    $scope.filtroProcedimento = $scope.filtrosProcedimentos[0];

    $scope.comoConheceu = [
        "Indicação",
        "Convênio",
        "Localização",
        "Internet",
        "Televisão",
        "Rádio"
    ];

    $scope.convenios = [
        "conv1",
        "conv2",
        "conv3"
    ];

    $("#mask_data_nasc").inputmask("d/m/y", {
        autoUnmask: true
    });

    $("#mask_cpf").inputmask("999.999.999-99", {
        placeholder: " ",
        clearMaskOnLostFocus: true
    });

    $("#mask_cep").inputmask("99999-999", {
        placeholder: " ",
        clearMaskOnLostFocus: true
    });

    $("#mask_telefone").inputmask("mask", {
        "mask": "(99) 9999-9999"
    });

    $("#mask_celular").inputmask("mask", {
        "mask": "(99) 99999-9999"
    });

    $(".mask_datetime").inputmask("d/m/y h:s", {
        autoUnmask: true
    });


    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.format = 'shortDate';

    $scope.loadingCep = false;
    $scope.buscaCep = function () {
        if ($scope.paciente.cep.length === 9) {
            console.log('buscaCep');
            var cep = $scope.paciente.cep.replace('-', '');
            $scope.loadingCep = true;
            $http({
                method: 'GET',
                url: 'http://cep.correiocontrol.com.br/' + cep + '.js'
            }).then(function successCallback(response) {
                $scope.loadingCep = false;
                console.log('buscaCep-successCallback');
                var data = response.data.replace('correiocontrolcep(', '');
                data = data.replace(')', '');
                data = data.replace(';', '');
                data = JSON.parse(data);
                if (!data.erro && data.logradouro != undefined) {
                    $scope.paciente.logradouro = data.logradouro;
                    $scope.paciente.cidade = data.localidade + ' - ' + data.uf;
                    $scope.paciente.bairro = data.bairro;
                }
            }, function errorCallback(response) {
                $scope.loadingCep = false;
                console.log('buscaCep-errorCallback');
            });
        } else {
            $scope.paciente.logradouro = "";
            $scope.paciente.cidade = "";
            $scope.paciente.bairro = "";
        }
    }

    $scope.tiposProcedimento = [
        {cod: '81000065', name: 'CONSULTA ODONTOLOGICA INICIAL', group: 'DIAGNOSTICO'},

        {cod: '85100048', name: 'COLAGEM DE FRAGMENTOS DENTARIOS', group: 'URGENCIA'},
        {cod: '81000049', name: 'CONSULTA ODONTOLOGICA DE URGENCIA', group: 'URGENCIA'},
        {cod: '82000468', name: 'CONTROLE DE HEMORRAGIA COM APLICACAO DE HEMOSTATIC', group: 'URGENCIA'},
        {cod: '82000484', name: 'CONTROLE DE HEMORRAGIA SEM APLICACAO DE HEMOSTATIC', group: 'URGENCIA'},
        {cod: '85000787', name: 'IMOBILIZACAO DENTARIA EM DENTES DECIDUOS', group: 'URGENCIA'},
        {cod: '85300020', name: 'IMOBILIZACAO DENTARIA EM DENTES PERMANENTES', group: 'URGENCIA'},
        {cod: '82001022', name: 'INCISAO E DRENAGEM EXTRA-ORAL DE ABSCESSO, HEMATO', group: 'URGENCIA'},
        {cod: '82001030', name: 'INCISAO E DRENAGEM INTRA-ORAL DE ABSCESSO, HEMATO', group: 'URGENCIA'},
        {cod: '85200034', name: 'PULPECTOMIA', group: 'URGENCIA'},
        {cod: '85400467', name: 'RECIMENTACAO DE TRABALHOS PROTETICOS', group: 'URGENCIA'},
        {cod: '82001170', name: 'REDUCAO CRUENTA DE FRATURA ALVEOLO DENTARIA', group: 'URGENCIA'},
        {cod: '82001189', name: 'REDUCAO INCRUENTA DE FRATURA ALVEOLO DENTARIA', group: 'URGENCIA'},
        {cod: '82001251', name: 'REIMPLANTE DENTARIO COM CONTENCAO', group: 'URGENCIA'},
        {cod: '85300063', name: 'TRATAMENTO DE ABSCESSO PERIODONTAL AGUDO', group: 'URGENCIA'},
        {cod: '82001650', name: 'TRATAMENTO DE ALVEOLITE', group: 'URGENCIA'},

        {cod: '81000375', name: 'RADIOGRAFIA INTERPROXIMAL - BITE-WING', group: 'RADIOLOGIA'},
        {cod: '81000383', name: 'RADIOGRAFIA OCLUSAL', group: 'RADIOLOGIA'},
        {cod: '81000421', name: 'RADIOGRAFIA PERIAPICAL', group: 'RADIOLOGIA'},

        {cod: '84000090', name: 'APLICACAO TOPICA DE FLUOR', group: 'PREVENCAO'},
        {cod: '84000139', name: 'ATIVIDADE EDUCATIVA EM SAUDE BUCAL', group: 'PREVENCAO'},
        {cod: '87000016', name: 'PAC. ESPECIAL-ATIVIDADE EDUCATIVA EM SAUDE BUCAL', group: 'PREVENCAO'},
        {cod: '84000198', name: 'PROFILAXIA POLIMENTO CORONARIO', group: 'PREVENCAO'},
        {cod: '84000236', name: 'TESTE DE CONTAGEM MICROBIOLOGICA', group: 'PREVENCAO'},
        {cod: '84000244', name: 'TESTE DE FLUXO SALIVAR', group: 'PREVENCAO'},
        {cod: '84000252', name: 'TESTE DE PH SALIVAR', group: 'PREVENCAO'},

        {cod: '84000031', name: 'APLICACAO DE CARIOSTATICO', group: 'ODONTOPEDIATRIA'},
        {cod: '84000058', name: 'APLICACAO DE SELANTE - TECNICA INVASIVA', group: 'ODONTOPEDIATRIA'},
        {cod: '84000074', name: 'APLICACAO DE SELANTE DE FOSSULAS E FISSURAS', group: 'ODONTOPEDIATRIA'},
        {cod: '81000014', name: 'CONDICIONAMENTO EM ODONTOLOGIA', group: 'ODONTOPEDIATRIA'},
        {cod: '87000032', name: 'CONDICIONAMENTO EM ODONTOLOGIA PARA PACIENTES COM', group: 'ODONTOPEDIATRIA'},
        {cod: '83000046', name: 'COROA DE ACO EM DENTE DECIDUO', group: 'ODONTOPEDIATRIA'},
        {cod: '87000059', name: 'COROA DE ACO EM DENTE PERMANENTE', group: 'ODONTOPEDIATRIA'},
        {cod: '83000062', name: 'COROA DE POLICARBONATO EM DENTE DECIDUO', group: 'ODONTOPEDIATRIA'},
        {cod: '87000067', name: 'COROA DE POLICARBONATO EM DENTE PERMANENTE', group: 'ODONTOPEDIATRIA'},
        {cod: '83000089', name: 'EXODONTIA SIMPLES DE DECIDUO', group: 'ODONTOPEDIATRIA'},
        {cod: '86000438', name: 'PISTAS DIRETAS DE PLANAS - SUPERIOR E INFERIOR', group: 'ODONTOPEDIATRIA'},
        {cod: '83000127', name: 'PULPOTOMIA EM DENTE DECIDUO', group: 'ODONTOPEDIATRIA'},
        {cod: '84000201', name: 'REMINERALIZACAO', group: 'ODONTOPEDIATRIA'},
        {cod: '85100137', name: 'REST. IONOMERO DE VIDRO - 1 FACE', group: 'ODONTOPEDIATRIA'},
        {cod: '85100145', name: 'REST. IONOMERO DE VIDRO - 2 FACES', group: 'ODONTOPEDIATRIA'},
        {cod: '85100153', name: 'REST. IONOMERO DE VIDRO - 3 FACES', group: 'ODONTOPEDIATRIA'},
        {cod: '85100161', name: 'REST. IONOMERO DE VIDRO - 4 FACES', group: 'ODONTOPEDIATRIA'},
        {cod: '83000135', name: 'RESTAURACAO ATRAUMATICA EM DENTE DECIDUO', group: 'ODONTOPEDIATRIA'},
        {cod: '83000151', name: 'TRATAMENTO ENDODONTICO EM DENTE DECIDUO', group: 'ODONTOPEDIATRIA'},

        {cod: '85100064', name: 'FACETA DIRETA EM RESINA FOTOPOLIMERIZAVEL', group: 'DENTISTICA'},
        {cod: '85400211', name: 'NUCLEO DE PREENCHIMENTO', group: 'DENTISTICA'},
        {cod: '85100099', name: 'REST AMALGAMA 1 FACE', group: 'DENTISTICA'},
        {cod: '85100102', name: 'REST AMALGAMA 2 FACES', group: 'DENTISTICA'},
        {cod: '85100110', name: 'REST AMALGAMA 3 FACES', group: 'DENTISTICA'},
        {cod: '85100129', name: 'REST AMALGAMA 4 FACES OU MAIS', group: 'DENTISTICA'},
        {cod: '85100196', name: 'REST RESINA FOTO 1 FACE', group: 'DENTISTICA'},
        {cod: '85100200', name: 'REST RESINA FOTO 2 FACES', group: 'DENTISTICA'},
        {cod: '85100218', name: 'REST RESINA FOTO 3 FACES', group: 'DENTISTICA'},
        {cod: '85100226', name: 'REST RESINA FOTO 4 FACES OU MAIS', group: 'DENTISTICA'},

        {cod: '82000026', name: 'ACOMPANHAMENTO DE TRATAMENTO/PROCEDIMENTO CIRURGI', group: 'PERIODONTIA'},
        {cod: '85400017', name: 'AJUSTE OCLUSAL POR ACRESCIMO', group: 'PERIODONTIA'},
        {cod: '85400025', name: 'AJUSTE OCLUSAL POR DESGASTE SELETIVO', group: 'PERIODONTIA'},
        {cod: '82000212', name: 'AUMENTO DE COROA CLINICA', group: 'PERIODONTIA'},
        {cod: '82000417', name: 'CIRURGIA PERIODONTAL A RETALHO', group: 'PERIODONTIA'},
        {cod: '82000557', name: 'CUNHA PROXIMAL', group: 'PERIODONTIA'},
        {cod: '85300012', name: 'DESSENSIBILIZACAO DENTARIA', group: 'PERIODONTIA'},
        {cod: '82000662', name: 'ENXERTO GENGIVAL LIVRE', group: 'PERIODONTIA'},
        {cod: '82000689', name: 'ENXERTO PEDICULADO', group: 'PERIODONTIA'},
        {cod: '82000921', name: 'GENGIVECTOMIA', group: 'PERIODONTIA'},
        {cod: '82000948', name: 'GENGIVOPLASTIA', group: 'PERIODONTIA'},
        {cod: '85300039', name: 'RASPAGEM SUB-GENGIVAL/ALISAMENTO RADICULAR', group: 'PERIODONTIA'},
        {cod: '85300047', name: 'RASPAGEM SUPRA-GENGIVAL', group: 'PERIODONTIA'},

        {cod: '82000034', name: 'ALVEOLOPLASTIA', group: 'CIRURGIA'},
        {cod: '82000050', name: 'AMPUTACAO RADICULAR COM OBTURACAO RETROGRADA', group: 'CIRURGIA'},
        {cod: '82000069', name: 'AMPUTACAO RADICULAR SEM OBTURACAO RETROGRADA', group: 'CIRURGIA'},
        {cod: '82000077', name: 'APICETOMIA BIRRADICULARES COM OBTURACAO RETROGRAD', group: 'CIRURGIA'},
        {cod: '82000085', name: 'APICETOMIA BIRRADICULARES SEM OBTURACAO RETROGRAD', group: 'CIRURGIA'},
        {cod: '82000158', name: 'APICETOMIA MULTIRRADICULARES COM OBTURACAO RETROG', group: 'CIRURGIA'},
        {cod: '82000166', name: 'APICETOMIA MULTIRRADICULARES SEM OBTURACAO RETROG', group: 'CIRURGIA'},
        {cod: '82000174', name: 'APICETOMIA UNIRRADICULARES COM OBTURACAO RETROGRA', group: 'CIRURGIA'},
        {cod: '82000182', name: 'APICETOMIA UNIRRADICULARES SEM OBTURACAO RETROGRA', group: 'CIRURGIA'},
        {cod: '82000190', name: 'APROFUNDAMENTO/AUMENTO DE VESTIBULO', group: 'CIRURGIA'},
        {cod: '82000239', name: 'BIOPSIA DE BOCA', group: 'CIRURGIA'},
        {cod: '88000133', name: 'BIOPSIA DE GLANDULA SALIVAR', group: 'CIRURGIA'},
        {cod: '82000255', name: 'BIOPSIA DE LABIO', group: 'CIRURGIA'},
        {cod: '82000263', name: 'BIOPSIA DE LINGUA', group: 'CIRURGIA'},
        {cod: '82000271', name: 'BIOPSIA DE MANDIBULA', group: 'CIRURGIA'},
        {cod: '82000280', name: 'BIOPSIA DE MAXILA', group: 'CIRURGIA'},
        {cod: '82000298', name: 'BRIDECTOMIA', group: 'CIRURGIA'},
        {cod: '82000301', name: 'BRIDOTOMIA', group: 'CIRURGIA'},
        {cod: '82000352', name: 'CIRURGIA PARA EXOSTOSE MAXILAR', group: 'CIRURGIA'},
        {cod: '82000360', name: 'CIRURGIA PARA TORUS MANDIBULAR BILATERAL', group: 'CIRURGIA'},
        {cod: '82000387', name: 'CIRURGIA PARA TORUS MANDIBULAR UNILATERAL', group: 'CIRURGIA'},
        {cod: '82000395', name: 'CIRURGIA PARA TORUS PALATINO', group: 'CIRURGIA'},
        {cod: '82000441', name: 'COLETA DE RASPADO EM LESOES OU SITIOS ESPECIFICOS', group: 'CIRURGIA'},
        {cod: '81000111', name: 'DIAGNOSTICO ANATOMO EM CITOLOGIA ESFOLIATIVA NA R', group: 'CIRURGIA'},
        {cod: '81000138', name: 'DIAGNOSTICO ANATOMO EM MATERIAL DE BIOPSIA NA REG', group: 'CIRURGIA'},
        {cod: '81000154', name: 'DIAGNOSTICO ANATOMO EM PECA CIRURGICA NA REGIAO B', group: 'CIRURGIA'},
        {cod: '81000170', name: 'DIAGNOSTICO ANATOMO EM PUNCAO NA REGIAO BMF', group: 'CIRURGIA'},
        {cod: '82000778', name: 'EXERESE OU EXCISAO DE CALCULO SALIVAR', group: 'CIRURGIA'},
        {cod: '82000786', name: 'EXERESE OU EXCISAO DE CISTOS ODONTOLOGICOS', group: 'CIRURGIA'},
        {cod: '82000794', name: 'EXERESE OU EXCISAO DE MUCOCELE', group: 'CIRURGIA'},
        {cod: '82000808', name: 'EXERESE OU EXCISAO DE RANULA', group: 'CIRURGIA'},
        {cod: '82000816', name: 'EXODONTIA A RETALHO', group: 'CIRURGIA'},
        {cod: '82000859', name: 'EXODONTIA DE RAIZ RESIDUAL', group: 'CIRURGIA'},
        {cod: '82000875', name: 'EXODONTIA SIMPLES DE PERMANENTE', group: 'CIRURGIA'},
        {cod: '82000883', name: 'FRENULECTOMIA LABIAL', group: 'CIRURGIA'},
        {cod: '82000891', name: 'FRENULECTOMIA LINGUAL', group: 'CIRURGIA'},
        {cod: '82000905', name: 'FRENULOTOMIA LABIAL', group: 'CIRURGIA'},
        {cod: '82000913', name: 'FRENULOTOMIA LINGUAL', group: 'CIRURGIA'},
        {cod: '82001073', name: 'ODONTO-SECCAO', group: 'CIRURGIA'},
        {cod: '82001103', name: 'PUNCAO ASPIRATIVA NA REGIAO BMF', group: 'CIRURGIA'},
        {cod: '82001154', name: 'RECONSTRUCAO DE SULCO GENGIVO-LABIAL', group: 'CIRURGIA'},
        {cod: '82001286', name: 'REMOCAO DE DENTES INCLUSOS / IMPACTADOS', group: 'CIRURGIA'},
        {cod: '82001294', name: 'REMOCAO DE DENTES SEMI-INCLUSOS / IMPACTADOS', group: 'CIRURGIA'},
        {cod: '82001391', name: 'RETIRADA DE CORPO ESTRANHO OROANTRAL OU ORONASAL D', group: 'CIRURGIA'},
        {cod: '82001464', name: 'SEPULTAMENTO RADICULAR', group: 'CIRURGIA'},
        {cod: '82001502', name: 'TRACIONAMENTO CIRURGICO COM FINALIDADE ORTODONTICA', group: 'CIRURGIA'},
        {cod: '82001510', name: 'TRAT CIRURGICO DAS FISTULAS BUCO NASAL', group: 'CIRURGIA'},
        {cod: '82001529', name: 'TRATAMENTO CIRURGICO DAS FISTULAS BUCO SINUSAL', group: 'CIRURGIA'},
        {cod: '82001545', name: 'TRATAMENTO CIRURGICO DE BRIDAS CONSTRITIVAS DA REG', group: 'CIRURGIA'},
        {cod: '82001553', name: 'TRATAMENTO CIRURGICO DE HIPERPLASIAS DE TECIDOS MO', group: 'CIRURGIA'},
        {cod: '82001588', name: 'TRATAMENTO CIRURGICO DE HIPERPLASIAS DE TECIDOS OS', group: 'CIRURGIA'},
        {cod: '82001596', name: 'TRATAMENTO CIRURGICO DE TUMORES BENIGNOS DE TECID', group: 'CIRURGIA'},
        {cod: '82001618', name: 'TRATAMENTO CIRURGICO DOS TUMORES BENIGNOS DE TECID', group: 'CIRURGIA'},
        {cod: '82001634', name: 'TRATAMENTO CIRURGICO PARA TUMORES ODONTOGENICOS BE', group: 'CIRURGIA'},
        {cod: '82001685', name: 'TUNELIZACAO', group: 'CIRURGIA'},
        {cod: '82001707', name: 'ULECTOMIA', group: 'CIRURGIA'},
        {cod: '82001715', name: 'ULOTOMIA', group: 'CIRURGIA'},

        {cod: '85200018', name: 'CLAREAMENTO DE DENTE DESVITALIZADO ANTERIOR', group: 'ENDODONTIA'},
        {cod: '85200093', name: 'RETRATAMENTO ENDODONTICO BIRRADICULAR', group: 'ENDODONTIA'},
        {cod: '85200107', name: 'RETRATAMENTO ENDODONTICO MULTIRRADICULAR', group: 'ENDODONTIA'},
        {cod: '85200115', name: 'RETRATAMENTO ENDODONTICO UNIRRADICULAR', group: 'ENDODONTIA'},
        {cod: '85200123', name: 'TRATAMENTO DE PERFURACAO ENDODONTICA', group: 'ENDODONTIA'},
        {cod: '85200140', name: 'TRATAMENTO ENDODONTICO BIRRADICULAR', group: 'ENDODONTIA'},
        {cod: '85200131', name: 'TRATAMENTO ENDODONTICO DE DENTE COM RIZOGENESE IN', group: 'ENDODONTIA'},
        {cod: '85200158', name: 'TRATAMENTO ENDODONTICO MULTIRRADICULAR', group: 'ENDODONTIA'},
        {cod: '85200166', name: 'TRATAMENTO ENDODONTICO UNIRRADICULAR', group: 'ENDODONTIA'}
    ];

    $scope.openModalTiposProcedimento = function (size) {
        var modalInstance = $modal.open(
            {
                templateUrl: 'views/modalTiposProcedimento.html',
                controller: 'PacienteController',
                size: size
            });
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

});
