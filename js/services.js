MetronicApp.factory('PacienteService', ['$resource',
        function($resource) {
            return $resource('http://localhost:8080/login', {
                id: '@id'
            }, {
                findListValues: {
                    method: 'GET',
                    isArray: true,
                    params: {
                        service: 'findListValues'
                    }
                },
                getPaciente: {
                    method: 'GET',
                    isArray: false,
                    params: {
                        service: 'findListValuesByParent'
                    }
                }
            });
        }
    ]);
